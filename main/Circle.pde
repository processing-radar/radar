public class Circle {

  int centerX, centerY, x, y, d;
  color rgb;

  public Circle(int centerX, int centerY, int radius, color rgb) throws Exception {
    if (radius < 1)
      throw new Exception("The radius must be bigger than 0");
    
    this.centerX = centerX;
    this.centerY = centerY;

    x = 0;
    y = radius;

    d = (5 - radius * 4) / 4;

    this.rgb = rgb > color(0, 0, 0) ? rgb : Color.black;
  }

  // Draw a circle
  public void drawCircle() {
    Point point = new Point(x, y, size, rgb);
    while (x <= y) {
      drawPixel(point);
      if (d < 0)
        d += 2 * x + 1;
      else {
        d += 2 * (x - y) + 1;
        y--;
      }
      x++;
    }
  }

  // Draw a point in all the octets
  private void drawPixel(Point point) {
    for (int i = 0; i < 4; i++) {
      point.updatePos(centerX + x, centerY + y);
      point.drawPoint();
      point.updatePos(centerX + y, centerY + x);
      point.drawPoint();
      
      if (i == 0 || i == 2) x *= -1;
      else y *= -1;
    }
  }
}
