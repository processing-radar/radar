public static class Color {

  // Neutral colors
  final static color black = #000000;
  final static color white = #FFFFFF;

  // Primary colors
  final static color red = #FF0000;
  final static color green = #00FF00;
  final static color blue = #0000FF;

  // Secondary colors
  final static color yellow = #FFFF00;
  final static color aqua = #00FFFF;
  final static color fuchsia = #FF00FF;
}
