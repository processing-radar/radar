public class Line {

  int x1, y1, x2, y2, size;
  color rgb;

  private int d, dx, dy, dx2, dy2, x, y, ix, iy;

  public Line(int x1, int y1, int x2, int y2, int size, color rgb) throws Exception {
    if (x1 == x2 && y1 == y2)
      throw new Exception("The coordinates of the points must be different");

    this.x1 = x1;
    this.y1 = y1;

    this.x2 = x2;
    this.y2 = y2;

    this.size = size > 1 ? size : 1;
    this.rgb = rgb > color(0, 0, 0) ? rgb : Color.black;

    prepare();
  }

  // Draw a line
  private void drawLine() {
    Point point = new Point(x, y, size, rgb);
    
    if (dx >= dy) othersDegrees(point);
    else ninetyDegrees(point);
  }

  // Draw a line with an angle different from ninety degrees
  private void othersDegrees(Point point) {
    while (x != x2) {
      point.updatePos(x, y);
      point.drawPoint();
      x += ix;
      d += dy2;
      if (d > dx) {
        y += iy;
        d -= dx2;
      }
    }
  }

  // Draw a line with an angle equal to ninety degrees
  private void ninetyDegrees(Point point) {
    while (y != y2) {
      point.updatePos(x, y);
      point.drawPoint();
      y += iy;
      d += dx2;
      if (d > dy) {
        x += ix;
        d -= dy2;
      }
    }
  }

  // Prepare the variables
  private void prepare() {
    // Delta of exact value and rounded value of the dependant variable
    d = 0;
     
    dx = Math.abs(x2 - x1);
    dy = Math.abs(y2 - y1);
    
    dx2 = 2 * dx; // Slope scaling factors to
    dy2 = 2 * dy; // avoid floating point
     
    ix = x1 < x2 ? 1 : -1; // Increment directino
    iy = y1 < y2 ? 1 : -1;
    
    x = x1;
    y = y1;
  }
}
