public class Point {

  int size;
  color rgb;

  private int x, y;

  public Point(int x, int y, int size, color rgb) {
    this.x = x;
    this.y = y;

    this.size = size > 1 ? size : 1;
    this.rgb = rgb > color(0, 0, 0) ? rgb : Color.black;
  }

  // Draw a point
  public void drawPoint() {
    strokeWeight(size);
    stroke(rgb);
    point(x, y);
  }

  // Update the position of the point
  public void updatePos(int x, int y) {
    this.x = x;
    this.y = y;
  }
}
