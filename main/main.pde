// Start position and size
int x1 = 200;
int y1 = 200;
int size = 5;

// Primitive forms
private Line[] lines = new Line[8];
private Circle circle;

void setup() {
  size(400, 400);
  background(Color.black);

  try { 
    circle = new Circle(200, 200, 50, Color.red);
  } catch (Exception e) {
    e.printStackTrace();
    exit();
  }

  //populateLines();
}

void draw() {
  circle.drawCircle();

  //for (Line t : lines) {
  //  t.drawLine();
  //}
}

private void populateLines() {
  try {
    lines[0] = new Line(x1, y1, 0, 0, size, Color.black); 
    lines[1] = new Line(x1, y1, 200, 0, size, Color.white); 
    lines[2] = new Line(x1, y1, 400, 0, size, Color.red); 
    lines[3] = new Line(x1, y1, 400, 200, size, Color.green); 
    lines[4] = new Line(x1, y1, 400, 400, size, Color.blue); 
    lines[5] = new Line(x1, y1, 200, 400, size, Color.yellow); 
    lines[6] = new Line(x1, y1, 0, 400, size, Color.aqua); 
    lines[7] = new Line(x1, y1, 0, 200, size, Color.fuchsia);
  } 
  catch (Exception e) {
    e.printStackTrace();
    exit();
  }
}
